all:
	mvn install

clean:
	mvn clean

show:
	find ~/.m2 -type f -name "javautils*" -exec ls -al {} \;

cleanrepo:
	./clean-repo
