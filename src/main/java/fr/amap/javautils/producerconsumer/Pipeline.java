package fr.amap.javautils.producerconsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

public class Pipeline<T>
{
    private List<Producer<T>> producers;

    private List<Consumer<T>> consumers;

    private BlockingQueue<T> queue;

    private static Executor executorService;

    public static boolean useExecutorService = true;

    /**
     * barrier used to synchronise consumers/producers on completion
     */
    private CyclicBarrier barrier;

    /**
     * number of non completed producers
     */
    private AtomicInteger activeProducers;

    private List<Throwable> errors;

    public Pipeline( int queueLength )
    {
        if ( queueLength < 1 )
            throw new IllegalArgumentException( "invalid queue length" );

        queue = new ArrayBlockingQueue<>( queueLength );
        producers = new ArrayList<>();
        consumers = new ArrayList<>();
    }

    BlockingQueue<T> getQueue()
    {
        return queue;
    }

    void addProducer( Producer<T> prod )
    {
        producers.add( prod );
    }

    void addConsumer( Consumer<T> cons )
    {
        consumers.add( cons );
    }

    public void run() throws Throwable
    {
        errors = null;
        start();
        waitBarrier();

        // relay errors

        for ( Actor<T> a : producers )
            if ( a.getLastError() != null )
                throw a.getLastError();

        for ( Actor<T> a : consumers )
            if ( a.getLastError() != null )
                throw a.getLastError();
    }

    public List<Throwable> getErrors()
    {
        if ( errors == null )
        {
            errors = new ArrayList<>();

            for ( Consumer<T> c : consumers )
                if ( c.getLastError() != null )
                    errors.add( c.getLastError() );

            for ( Producer<T> p : producers )
                if ( p.getLastError() != null )
                    errors.add( p.getLastError() );
        }

        return errors;
    }

    private void start() throws Exception
    {
        if ( consumers.size() == 0 || producers.size() == 0 )
            throw new Exception( "no producer/consumer in pipeline" );

        activeProducers = new AtomicInteger( producers.size() );

        if ( useExecutorService )
        {
            int n = consumers.size() + producers.size();
            getExecutor( n );

            // start consumers
            for ( Consumer<T> c : consumers )
                executorService.execute( c );

            // start producers
            for ( Producer<T> p : producers )
                executorService.execute( p );
        }
        else
        {
            // start consumers
            for ( Consumer<T> c : consumers )
                new Thread( c ).start();

            // start producers
            for ( Producer<T> p : producers )
                new Thread( p ).start();
        }
    }

    private static Executor getExecutor( int nThreads ) throws Exception
    {
        if ( nThreads < 1 )
            throw new Exception( "invalid thread count: " + nThreads );

        boolean create = executorService == null;
        if ( !create )
            create = executorService.getThreadCount() != nThreads;

        if ( create )
        {
            if ( executorService != null )
                executorService.shutdown();
            executorService = new Executor( nThreads ); // create a pool of threads
        }

        return executorService;
    }

    public static void shutdownExecutorService() throws InterruptedException
    {
        if ( executorService != null )
        {
            executorService.shutdown();
            executorService = null;
        }
    }

    public void terminateActors()
    {
        // stop producers
        for ( Actor<T> a : producers )
            a.terminate();

        // stop consumers
        for ( Actor<T> a : consumers )
            a.terminate();
    }

    void waitBarrier() throws InterruptedException, BrokenBarrierException
    {
        getBarrier().await();
    }

    private synchronized CyclicBarrier getBarrier()
    {
        int n = consumers.size() + producers.size() + 1;

        if ( barrier == null || barrier.getParties() != n )
            barrier = new CyclicBarrier( n );

        return barrier;
    }

    /**
     * called when a producer has completed producing
     * @throws Exception
     */
    void producerCompleted()
    {
        int n = activeProducers.decrementAndGet();
        if ( n < 0 )
            throw new Error( "invalid active producer count " + n );
    }

    /**
     * @return true if not all producers have completed and tasks remain in the queue
     */
    synchronized boolean hasRemainingTasks()
    {
        if ( activeProducers.get() != 0 )
            return true;

        return !queue.isEmpty();
    }
}
