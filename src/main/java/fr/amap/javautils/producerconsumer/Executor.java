package fr.amap.javautils.producerconsumer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Executor
{
    private ExecutorService executorService;

    /**
     * thread count for the ExecutorService
     */
    private int threadCount;

    public Executor( int nThread )
    {
        threadCount = nThread;
        executorService = Executors.newFixedThreadPool( threadCount );
    }

    public int getThreadCount()
    {
        return threadCount;
    }

    public void execute( Runnable r )
    {
        executorService.execute( r );
    }

    public void shutdown() throws InterruptedException
    {
        if ( executorService != null )
        {
            executorService.shutdown();
            while ( !executorService.isTerminated() )
                executorService.awaitTermination( 1, TimeUnit.MILLISECONDS );
            executorService = null;
        }
    }
}
