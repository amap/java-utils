package fr.amap.javautils.producerconsumer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public abstract class Consumer<T> extends Actor<T> implements Runnable
{
    private final BlockingQueue<T> queue;

    private long pollTimeoutDuration = 1;

    private TimeUnit pollTimeoutUnit = TimeUnit.MILLISECONDS;

    public Consumer( Pipeline<T> p )
    {
        super( p );
        queue = p.getQueue();
        p.addConsumer( this );
    }

    @Override
    public void run()
    {
        terminate.set( false );

        while ( !terminate.get() && pipeline.hasRemainingTasks() )
            try
            {
                T o = queue.poll( pollTimeoutDuration, pollTimeoutUnit );
                if ( o != null )
                    consume( o );
            }
            catch( Throwable e )
            {
                lastError = e;
                break;
            }

        onTerminate();

        try
        {
            pipeline.waitBarrier();
        }
        catch( Throwable e )
        {
            if ( lastError == null ) // keep original error
                lastError = e;
        }
    }

    public void setPollTimeoutDuration( long dur )
    {
        pollTimeoutDuration = dur;
    }

    public void setPollTimeoutUnit( TimeUnit unit )
    {
        pollTimeoutUnit = unit;
    }

    protected abstract void consume( T x );
}
