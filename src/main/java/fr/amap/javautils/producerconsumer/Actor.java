package fr.amap.javautils.producerconsumer;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class Actor<T>
{
    /**
     * owning pipeline
     */
    protected Pipeline<T> pipeline;

    /**
     * true to request actor to quit processing loop (run() method)
     */
    protected AtomicBoolean terminate;

    /**
     * last met error
     */
    protected Throwable lastError;

    public Actor( Pipeline<T> p )
    {
        pipeline = p;
        terminate = new AtomicBoolean( false );
    }

    /**
     * request actor to interrupt processing loop
     */
    public void terminate()
    {
        terminate.lazySet( true );
    }

    public Throwable getLastError()
    {
        return lastError;
    }

    protected void onTerminate()
    {
    }
}
