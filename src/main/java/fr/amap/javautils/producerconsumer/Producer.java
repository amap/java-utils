package fr.amap.javautils.producerconsumer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public abstract class Producer<T> extends Actor<T> implements Runnable
{
    private final BlockingQueue<T> queue;

    /**
     * number of queue "offer" attempts before issuing an error
     */
    private int offerRetries = 100;

    /**
     * time unit of an "offer" attempt
     */
    private TimeUnit offerUnit = TimeUnit.MILLISECONDS;

    public Producer( Pipeline<T> p )
    {
        super( p );
        queue = p.getQueue();
        p.addProducer( this );
    }

    @Override
    public void run()
    {
        terminate.set( false );

        while ( true )
            try
            {
                T p = produce();
                if ( p == null || terminate.get() )
                    break;

                boolean ok = false;
                for ( int i = 0; i < offerRetries; i++ )
                    if ( queue.offer( p, 1, offerUnit ) )
                    {
                        ok = true;
                        break;
                    }
                if ( !ok )
                {
                    lastError = new Exception( String.format( "Cannot add task to queue: timeout after %d %s", offerRetries, offerUnit ) );
                    break;
                }
            }
            catch( Throwable e )
            {
                lastError = e;
                break;
            }

        pipeline.producerCompleted();

        onTerminate();

        try
        {
            pipeline.waitBarrier();
        }
        catch( Throwable e )
        {
            if ( lastError == null ) // keep original error
                lastError = e;
        }
    }

    public void setOfferRetries( int n )
    {
        if ( n <= 0 )
            throw new IllegalArgumentException( "invalid offer retry count " + n );
        offerRetries = n;
    }

    public void setOfferUnit( TimeUnit unit )
    {
        offerUnit = unit;
    }

    protected abstract T produce();
}
