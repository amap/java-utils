package fr.amap.javautils.array;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.function.BiConsumer;

/**
 * array manager (reallocation, sort, dichotomic search, ...)
 * @author grand
 */
public class Array<T> implements Iterable<T>
{
    private T[] array;

    /**
     * real length, ie. index of last element + 1
     */
    private int len;

    /**
     * true if the array is sorted with the custom comparator (not the ascending comparator)
     */
    private boolean sorted;

    /**
     * true if the array is sorted with the ascending comparator
     */
    private boolean ascendSorted;

    /**
     * true if the array length fits its allocation size
     */
    private boolean fitted;

    /**
     * chunk size of reallocation
     */
    private int chunk = 100;

    /**
     * custom comparator used by sort()
     */
    private Comparator<T> comparator;

    /**
     * comparator used by sortAscending()
     */
    private Comparator<T> ascendComparator;

    /**
     * used by dicoSearch(), lower limit of search interval
     */
    private int inf;

    /**
     * used by dicoSearch(), upper limit of search interval
     */
    private int sup;

    public Array( T[] arr )
    {
        array = arr;
        sorted = false;
        ascendSorted = false;
        len = arr.length;

        // update real length in case nulls are at the end of the given array
        int i = len - 1;
        while ( i >= 0 && array[ i ] == null )
            i--;
        len = i + 1;

        fitted = len == arr.length;
    }

    private void realloc()
    {
        if ( array.length == len )
        {
            array = Arrays.copyOf( array, len + chunk );
            fitted = false;
        }
    }

    private void realloc( int index )
    {
        if ( array.length <= index )
        {
            int newLen = index / chunk;
            newLen++;
            newLen *= chunk;
            array = Arrays.copyOf( array, newLen );
            fitted = false;
            len = index + 1;
        }
    }

    public int append( T elem )
    {
        realloc();
        array[ len++ ] = elem;
        sorted = false;
        ascendSorted = false;
        return len - 1;
    }

    public void append( Array<T> arr )
    {
        int oldLen = len;
        realloc( len + arr.len - 1 );
        System.arraycopy( arr.array, 0, array, oldLen, arr.len );
        len = oldLen + arr.len;
    }

    public void setAt( T elem, int index )
    {
        realloc( index );
        array[ index ] = elem;
        //        len = Math.max( len, index );
        sorted = false;
        ascendSorted = false;
    }

    /**
     * get real element count
     */
    public int getLength()
    {
        return len;
    }

    public T[] getArray()
    {
        return array;
    }

    /**
     * ajust array allocation to real length
     */
    public void fit()
    {
        if ( !fitted )
        {
            array = Arrays.copyOf( array, len );
            fitted = true;
        }
    }

    public void sort( Comparator<T> comp )
    {
        if ( !sorted )
        {
            fit();
            Arrays.parallelSort( array, comp );
            sorted = true;
        }
    }

    public void sort()
    {
        if ( !sorted )
        {
            fit();
            Arrays.parallelSort( array, comparator );
            sorted = true;
        }
    }

    public void sortAscending()
    {
        if ( ascendComparator == null )
            throw new Error( "please set an ascending comparator" );
        //fit();
        if ( !ascendSorted )
        {
            if ( len > 1 )
                Arrays.parallelSort( array, 0, len, ascendComparator );
            sorted = false; // conservative, the custom comparator MAY NOT be ascending
            ascendSorted = true;
        }
    }

    public boolean isSortedAscending()
    {
        if ( ascendComparator == null )
            throw new Error( "please set an ascending comparator" );

        T prev = null;
        for ( T elem : array )
        {
            if ( prev != null && elem != null )
                if ( ascendComparator.compare( prev, elem ) >= 0 )
                    return false;
            prev = elem;
        }
        return true;
    }

    public void setChunkSize( int s )
    {
        if ( s < 1 )
            throw new Error( "invalid chunk size : " + s );
        chunk = s;
    }

    /**
     * set element custom comparator
     */
    public void setComparator( Comparator<T> comp )
    {
        comparator = comp;
    }

    /**
     * set element ascending comparator
     */
    public void setAscendingComparator( Comparator<T> comp )
    {
        ascendComparator = comp;
    }

    /**
     * set element known to be missing in the array at position returned by previous dicoSearch() call
     * WARNING : dicoSearch() MUST have been called before on element passed as parameter !!
     * WARNING : NOT thread safe !!
     */
    public void insertMissing( T elem )
    {
        realloc();
        if ( len > inf )
            System.arraycopy( array, inf, array, inf + 1, len - inf );
        array[ inf ] = elem;
        len++;
    }

    public void insertSorted( T elem, boolean replaceExisting )
    {
        int i = dicoSearch( elem );
        if ( i == -1 )
            insertMissing( elem );
        else if ( replaceExisting )
            array[ i ] = elem;
    }

    /**
     * dichotomic search, find the index of the first array element == target 
     * @return element index, -1 if not found (in this case, inf==sup==position where the element would be if it was in the array)
     */
    public int dicoSearch( T target )
    {
        sortAscending();

        inf = 0;
        sup = len;
        while ( inf < sup )
        {
            int i = (inf + sup) / 2;
            T curr = array[ i ];
            int c = ascendComparator.compare( target, curr );
            if ( c == 0 )
                return i;

            if ( inf == sup )
                return -1;

            if ( c < 0 )
                sup = i;
            else
                inf = i + 1;
        }
        return -1;
    }

    @Override
    public Iterator<T> iterator()
    {
        return Arrays.stream( array ).iterator();
    }

    public static void main( String[] args )
    {
        //        test1();
        //        test2();
        //        test3();
        //        test4();
        test6();
    }

    private static void test5()
    {
        Integer[] arr = {};
        Array<Integer> a = new Array<>( arr );
        a.setChunkSize( 3 );
        a.setAt( Integer.valueOf( 0 ), 1 );
        if ( a.getLength() != 2 )
            throw new Error();
    }

    private static void test4()
    {
        Integer[] arr = {};
        Array<Integer> a = new Array<>( arr );
        a.setChunkSize( 3 );
        a.setAt( Integer.valueOf( 0 ), 0 );
        if ( a.getLength() != 1 )
            throw new Error();
    }

    private static void test3()
    {
        // insertSorted

        Integer[] arr = {};
        Array<Integer> a = new Array<>( arr );
        a.setChunkSize( 3 );
        a.setAscendingComparator( ( i1, i2 ) -> {
            if ( i1 < i2 )
                return -1;
            if ( i1 > i2 )
                return 1;
            return 0;
        } );

        BiConsumer<Array<Integer>, int[]> check = ( arr1, arr2 ) -> {
            if ( arr1.getLength() != arr2.length )
                throw new Error();
            for ( int i = arr2.length - 1; i >= 0; i-- )
                if ( arr1.array[ i ] != arr2[ i ] )
                    throw new Error();
        };

        check.accept( a, new int[] {} );
        a.insertSorted( 5, false );
        check.accept( a, new int[] { 5 } );
        a.insertSorted( 0, false );
        check.accept( a, new int[] { 0, 5 } );
        a.insertSorted( 1, false );
        check.accept( a, new int[] { 0, 1, 5 } );
        a.insertSorted( 10, false );
        check.accept( a, new int[] { 0, 1, 5, 10 } );
        a.insertSorted( 5, false );
        check.accept( a, new int[] { 0, 1, 5, 10 } );
        a.insertSorted( 5, true );
        check.accept( a, new int[] { 0, 1, 5, 10 } );

        System.out.println( "OK" );
    }

    private static void test1()
    {
        // odd element count

        Integer[] arr = {};
        Array<Integer> a = new Array<>( arr );
        a.setChunkSize( 2 );
        a.setAscendingComparator( ( i1, i2 ) -> {
            if ( i1 < i2 )
                return -1;
            if ( i1 > i2 )
                return 1;
            return 0;
        } );
        a.append( 7 );
        a.append( 10 );
        a.append( 1 );
        a.append( 5 );
        a.append( 2 );
        //a.sort();
        a.fit();

        //System.out.println( Arrays.toString( a.getArray() ) );
        if ( a.dicoSearch( 1 ) != 0 )
            throw new Error();
        if ( a.dicoSearch( 2 ) != 1 )
            throw new Error();
        if ( a.dicoSearch( 5 ) != 2 )
            throw new Error();
        if ( a.dicoSearch( 7 ) != 3 )
            throw new Error();
        if ( a.dicoSearch( 10 ) != 4 )
            throw new Error();
        if ( a.dicoSearch( 11 ) != -1 )
            throw new Error();
        if ( a.dicoSearch( -2 ) != -1 )
            throw new Error();

        System.out.println( "OK" );
    }

    private static void test2()
    {
        // even element count

        Integer[] arr = {};
        Array<Integer> a = new Array<>( arr );
        a.setAscendingComparator( ( i1, i2 ) -> {
            if ( i1 < i2 )
                return -1;
            if ( i1 > i2 )
                return 1;
            return 0;
        } );
        a.append( 7 );
        a.append( 10 );
        a.append( 1 );
        a.append( 12 );
        a.append( 5 );
        a.append( 2 );

        System.out.println( Arrays.toString( a.getArray() ) );
        if ( a.dicoSearch( 1 ) != 0 )
            throw new Error();
        if ( a.dicoSearch( 2 ) != 1 )
            throw new Error();
        if ( a.dicoSearch( 5 ) != 2 )
            throw new Error();
        if ( a.dicoSearch( 7 ) != 3 )
            throw new Error();
        if ( a.dicoSearch( 10 ) != 4 )
            throw new Error();
        if ( a.dicoSearch( 12 ) != 5 )
            throw new Error();
        a.fit();
        System.out.println( Arrays.toString( a.getArray() ) );

        System.out.println( "OK" );
    }

    private static void test6()
    {
        Integer[] arr = { 0, 1, 2 };
        Array<Integer> a = new Array<>( arr );
        a.setChunkSize( 1 );
        a.append( 3 );
        Integer[] arr2 = { 4, 5, null, null };
        Array<Integer> a2 = new Array<>( arr2 );
        a.append( a2 );

        if ( a.getLength() != 6 )
            throw new Error();

        for ( int i = 0; i < 6; i++ )
            if ( a.array[ i ] != i )
                throw new Error();

        System.out.println( "OK" );
    }
}
