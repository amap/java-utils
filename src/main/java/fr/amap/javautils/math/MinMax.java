package fr.amap.javautils.math;

public class MinMax
{
    private double min;

    private double max;

    public MinMax()
    {
        min = Double.POSITIVE_INFINITY;
        max = Double.NEGATIVE_INFINITY;
    }

    public void minmax( double v )
    {
        if ( !Double.isNaN( v ) )
            min = Math.min( min, v );
        if ( !Double.isNaN( v ) )
            max = Math.max( max, v );
    }

    public void minmaxAny( double v )
    {
        min = Math.min( min, v );
        max = Math.max( max, v );
    }

    public double getMin()
    {
        return min;
    }

    public double getMax()
    {
        return max;
    }

    public double interpolate( double v )
    {
        return (v - min) / (max - min);
    }

    public void merge( MinMax mm )
    {
        min = Math.min( min, mm.min );
        max = Math.max( max, mm.max );
    }

    @Override
    public String toString()
    {
        return "min=" + min + ", max=" + max;
    }
}