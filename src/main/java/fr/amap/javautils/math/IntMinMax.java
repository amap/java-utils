package fr.amap.javautils.math;

public class IntMinMax
{
    private int min;

    private int max;

    public IntMinMax()
    {
        min = Integer.MAX_VALUE;
        max = Integer.MIN_VALUE;
    }

    public void minmax( int v )
    {
        min = Math.min( min, v );
        max = Math.max( max, v );
    }

    public int getMin()
    {
        return min;
    }

    public int getMax()
    {
        return max;
    }

    public void merge( IntMinMax mm )
    {
        min = Math.min( min, mm.min );
        max = Math.max( max, mm.max );
    }

    @Override
    public String toString()
    {
        return "min=" + min + ", max=" + max;
    }
}
