package fr.amap.javautils.voxelreader;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

public class VoxelSpaceInfos
{
    private Point3d minCorner;

    private Point3d maxCorner;

    private Point3i split;

    private Point3d voxelSize;

    public VoxelSpaceInfos( Point3d minCorner, Point3d maxCorner, Point3i split )
    {
        this.minCorner = minCorner;
        this.maxCorner = maxCorner;
        this.split = split;
    }

    private void updateVoxelSize()
    {
        double boundingBoxSizeX = maxCorner.x - minCorner.x;
        double boundingBoxSizeY = maxCorner.y - minCorner.y;
        double boundingBoxSizeZ = maxCorner.z - minCorner.z;

        this.voxelSize.x = boundingBoxSizeX / split.x;
        this.voxelSize.y = boundingBoxSizeY / split.y;
        this.voxelSize.z = boundingBoxSizeZ / split.z;
    }

    public Point3d getVoxelSize()
    {
        if ( voxelSize == null )
        {
            voxelSize = new Point3d();
            updateVoxelSize();
        }
        return voxelSize;
    }

    public Point3i getSplit()
    {
        return split;
    }

    public double getVoxelVolume()
    {
        Point3d s = getVoxelSize();
        return s.x * s.y * s.z;
    }
}
