package fr.amap.javautils.voxelreader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

import fr.amap.javautils.string.WordIterator;

public class VoxelSpaceReader implements Iterator<Voxel>, AutoCloseable
{
    private BufferedReader reader;

    private VoxelSpaceInfos voxelSpaceInfos;

    /**
     * next voxel file line
     */
    private String line;

    /**
     * voxel returned by next()
     */
    private Voxel nextVoxel;

    /**
     * column names
     */
    private String[] header;

    /**
     * columns of interest (which value is read among those present in input file)
     */
    private List<Column> readColumns;

    /**
     * index mapping from .vox column index to "readColumns" list
     */
    private int[] columnIndices;

    /**
     * word splitter/iterator
     */
    private WordIterator wi;

    /*
    VOXEL SPACE
    #min_corner: 769185.0 6289750.0 -2.0
    #max_corner: 769230.0 6289810.0 18.0
    #split: 225 300 100
    #type: TLS #res: 0.2 #MAX_PAD: 5.0 #LAD_TYPE: Spherical
    i j k PadBVTotal angleMean bvEntering bvIntercepted ground_distance lMeanTotal lgTotal nbEchos nbSampling transmittance bvPotential
    0 0 0 0 84.8300247 0.0070498 0 0.1 0.1565562 3.4442365 0 22 1 1.03565
    */
    public VoxelSpaceReader( String file, String col1name, String col2name ) throws FileNotFoundException, IOException
    {
        nextVoxel = new Voxel();
        reader = new BufferedReader( new FileReader( new File( file ) ) );
        readColumns = new ArrayList<>();
        wi = new WordIterator( ' ' );

        Point3d minCorner = null, maxCorner = null;
        Point3i split = null;
        int ni = 0, nj = 0, nk = 0;

        // VOXEL SPACE
        getNextLine( true );

        if ( !line.equals( "VOXEL SPACE" ) )
            throw new IOException( file + ": not an voxel file" );

        // information comments
        do
        {
            getNextLine( true );
            if ( line.startsWith( "#min_corner" ) )
            {
                String[] tmp = line.split( " " );
                double x = Double.valueOf( tmp[ 1 ] );
                double y = Double.valueOf( tmp[ 2 ] );
                double z = Double.valueOf( tmp[ 3 ] );
                minCorner = new Point3d( x, y, z );
            }
            else if ( line.startsWith( "#max_corner" ) )
            {
                String[] tmp = line.split( " " );
                double x = Double.valueOf( tmp[ 1 ] );
                double y = Double.valueOf( tmp[ 2 ] );
                double z = Double.valueOf( tmp[ 3 ] );
                maxCorner = new Point3d( x, y, z );
            }
            else if ( line.startsWith( "#split" ) )
            {
                String[] tmp = line.split( " " );
                ni = Integer.valueOf( tmp[ 1 ] );
                nj = Integer.valueOf( tmp[ 2 ] );
                nk = Integer.valueOf( tmp[ 3 ] );
                split = new Point3i( ni, nj, nk );
            }
            else if ( line.startsWith( "#type" ) )
            {
            }
            else if ( line.startsWith( "#LAD_TYPE" ) )
            {
            }
            else
                break;
        }
        while ( true );

        if ( minCorner == null || maxCorner == null || split == null )
            throw new IOException( file + ": not a valid voxel file" );

        voxelSpaceInfos = new VoxelSpaceInfos( minCorner, maxCorner, split );

        // header (i j k PadBVTotal ...)

        if ( !line.startsWith( "i j k " ) )
            throw new IOException( file + ": not a valid voxel file" );

        header = line.split( " " );

        addColumn( col1name );
        addColumn( col2name );
        nextVoxel.columns = readColumns;

        getNextLine( true );
    }

    public Column addColumn( String name )
    {
        Column exists = getColumn( name );
        if ( exists != null )
            return exists;

        Column col = parseColumn( name );
        if ( col == null )
            return null;

        readColumns.add( col );

        readColumns.sort( ( c1, c2 ) -> {
            if ( c1.index < c2.index )
                return -1;
            if ( c1.index > c2.index )
                return 1;
            return 0;
        } );

        columnIndices = new int[ header.length ];
        Arrays.fill( columnIndices, -1 );
        int i = 0;
        for ( Column c : readColumns )
            columnIndices[ c.index ] = i++;

        return col;
    }

    /**
     * parse column of interest
     * @return 
     */
    private Column parseColumn( String name )
    {
        int i = 0;
        for ( String col : header )
        {
            if ( col.equals( name ) )
                return new Column( name, i );
            i++;
        }

        return null;
    }

    /**
     * @return true if input header has given column
     */
    public boolean hasColumn( String col )
    {
        for ( String c : header )
            if ( c.equals( col ) )
                return true;
        return false;
    }

    public Column getColumn( String name )
    {
        for ( Column c : readColumns )
            if ( c.name.equals( name ) )
                return c;

        return null;
    }

    public void detectMinMaxValues()
    {
        for ( Column c : readColumns )
            c.detectMinMax();
    }

    public VoxelSpaceInfos getVoxelSpaceInfos()
    {
        return voxelSpaceInfos;
    }

    @Override
    public boolean hasNext()
    {
        if ( reader == null )
            return false;

        return getNextLine( false ) != null;
    }

    private String getNextLine( boolean force )
    {
        if ( force || line == null )
            try
            {
                line = reader.readLine();
            }
            catch( IOException e )
            {
                e.printStackTrace();
            }
        return line;
    }

    // Iterator<Voxel> interface

    @Override
    public Voxel next()
    {
        // i j k PadBVTotal angleMean bvEntering bvIntercepted ground_distance lMeanTotal lgTotal nbEchos nbSampling transmittance bvPotential

        wi.reset( line );
        while ( wi.hasNext() )
        {
            String val = wi.next();
            switch ( wi.index() )
            {
                case 0:
                    nextVoxel.i = Integer.valueOf( val );
                    break;

                case 1:
                    nextVoxel.j = Integer.valueOf( val );
                    break;

                case 2:
                    nextVoxel.k = Integer.valueOf( val );
                    break;

                default:
                    int i = wi.index();
                    int colInd = columnIndices[ i ];
                    if ( colInd != -1 )
                        readColumns.get( colInd ).value = Double.valueOf( val );
                    break;
            }
        }

        getNextLine( true );

        return nextVoxel;
    }

    // AutoCloseable interface

    @Override
    public void close() throws IOException
    {
        if ( reader != null )
        {
            reader.close();
            reader = null;
        }
    }
}
