package fr.amap.javautils.voxelreader;

import fr.amap.javautils.math.MinMax;

public class Column
{
    public final String name;

    /**
     * index in the value/header line 
     */
    public final int index;

    public final MinMax minmax;

    public double value;

    public Column( String name, int index )
    {
        this.name = name;
        this.index = index;
        minmax = new MinMax();
    }

    public void detectMinMax()
    {
        minmax.minmax( value );
    }
}
