package fr.amap.javautils.voxelreader;

import java.util.List;

public class Voxel
{
    public int i;

    public int j;

    public int k;

    public List<Column> columns;
}
