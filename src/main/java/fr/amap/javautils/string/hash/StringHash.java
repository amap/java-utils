package fr.amap.javautils.string.hash;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Encoder;

public class StringHash
{
    /**
     * input byte buffer
     */
    private byte[] data;

    /**
     * real content length
     */
    private int dataLen;

    private String hashAlgorithm = "SHA-512";

    public StringHash()
    {
        reset();
    }

    public void reset()
    {
        data = new byte[ 8192 ];
        dataLen = 0;
    }

    private void reallocate( int addedLen )
    {
        if ( addedLen + dataLen > data.length )
            data = Arrays.copyOf( data, addedLen + dataLen );
    }

    public void addString( String s )
    {
        addBytes( s.getBytes(), s.length() );
    }

    private void addBytes( byte[] bs, int len )
    {
        reallocate( bs.length );
        System.arraycopy( bs, 0, data, dataLen, len );
        dataLen += len;
    }

    public void addFile( String path ) throws FileNotFoundException, IOException
    {
        //        try (FileReader reader = new FileReader( path ); BufferedReader br = new BufferedReader( reader ))
        try (FileInputStream br = new FileInputStream( path ))
        {
            byte[] buf = new byte[ 1024 ];

            boolean end;
            do
            {
                int read = br.read( buf );
                end = read == -1;
                if ( !end )
                    addBytes( buf, read );
            }
            while ( !end );
        }
    }

    public String getHashString() throws NoSuchAlgorithmException
    {
        byte[] b = getHash();
        Encoder enc = Base64.getEncoder();
        return enc.encodeToString( b );
    }

    public byte[] getHash() throws NoSuchAlgorithmException
    {
        MessageDigest messageDigest = MessageDigest.getInstance( hashAlgorithm );
        byte[] b = data.length == dataLen ? data : Arrays.copyOf( data, dataLen );
        messageDigest.update( b );
        return messageDigest.digest();
    }

    private void log()
    {
        try (PrintWriter pw = new PrintWriter( new BufferedWriter( new FileWriter( "/tmp/hash" ) ) ))
        {
            pw.print( new String( Arrays.copyOf( data, dataLen ) ) );
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
    }

    public static void main( String[] args ) throws NoSuchAlgorithmException, FileNotFoundException, IOException
    {
        StringHash sh = new StringHash();
        //        sh.addString( "sdfgtopegokgrep" );
        sh.addFile( "/home/grand/.bashrc" );
        System.out.println( sh.getHashString() );
    }
}
