package fr.amap.javautils.string;

import java.util.Iterator;

/**
 * Word iterator on a string.
 * Words are separated by one (or more) separating character.
 * @author grand
 */
public class WordIterator implements Iterator<String>
{
    /**
     * source string
     */
    private String source;

    /**
     * word separator
     */
    private char separator;

    /**
     * source string length
     */
    private int len;

    /**
     * current char in source string
     */
    private char curr;

    /**
     * current char index in source string
     */
    private int charIndex;

    /**
     * current parsed word index
     */
    private int wordIndex;

    private StringBuilder sb;

    public WordIterator( char sep )
    {
        separator = sep;
        if ( sep == 0 )
            throw new Error( "WordIterator : invalid null separator" );
        sb = new StringBuilder();
    }

    public void reset( String s )
    {
        source = s;
        charIndex = -1;
        wordIndex = -1;
        len = s.length();
        sb.setLength( 0 );
        curr = separator;
    }

    public int index()
    {
        return wordIndex;
    }

    @Override
    public boolean hasNext()
    {
        while ( curr == separator )
        {
            if ( charIndex == len - 1 )
                return false;
            charIndex++;
            curr = source.charAt( charIndex );
        }
        return charIndex < len;
    }

    @Override
    public String next()
    {
        sb.setLength( 0 );
        while ( curr != separator )
        {
            sb.append( curr );
            charIndex++;
            if ( charIndex == len )
                break;
            curr = source.charAt( charIndex );
        }
        wordIndex++;
        return sb.toString();
    }
}
