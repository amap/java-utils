package fr.amap.javautils.format;

public class Time
{
    public static String durationToString( long dur )
    {
        StringBuilder sb = new StringBuilder();
        long m = dur / 60000;
        if ( m > 0 )
        {
            sb.append( m );
            sb.append( " min " );
        }
        dur -= m * 60000;

        long s = dur / 1000;
        if ( s > 0 )
        {
            sb.append( s );
            sb.append( " s " );
        }
        long ms = dur - s * 1000;
        if ( ms > 0 )
        {
            sb.append( ms );
            sb.append( " ms" );
        }

        return sb.toString();
    }
}
