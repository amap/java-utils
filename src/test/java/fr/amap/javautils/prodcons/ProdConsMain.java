package fr.amap.javautils.prodcons;

import java.util.concurrent.atomic.AtomicInteger;

import fr.amap.javautils.producerconsumer.Pipeline;

public class ProdConsMain
{
    public static void main( String[] args ) throws Throwable
    {
        //        int n = 10;
        //        for ( int i = 0; i < n; i++ )
        //        {
        //            System.out.println( (i + 1) + "/" + n + "..." );
        //            ProdConsMain.run2();
        //        }
        run4();

        System.out.println( "END" );
    }

    private static void run1() throws Throwable
    {
        Pipeline<String> pipeline = new Pipeline<>( 10 );
        AtomicInteger counter = new AtomicInteger( 0 );
        new TestProd( pipeline, counter );

        int n = 3;
        for ( int i = 0; i < n; i++ )
        {
            //            TestCons tc = new TestCons( pipeline.getQueue() );
            //            pipeline.addConsumer( tc );
            new TestCons( pipeline, counter );
        }

        pipeline.run();
        Pipeline.shutdownExecutorService();

        if ( counter.get() != 0 )
            throw new Exception();

        System.out.println( "OK" );
    }

    // multiple producer runs
    private static void run2() throws Throwable
    {
        Pipeline<String> pipeline = new Pipeline<>( 10 );
        AtomicInteger counter = new AtomicInteger( 0 );
        TestProd prod = new TestProd( pipeline, counter );

        int n = 3;
        for ( int i = 0; i < n; i++ )
        {
            //            TestCons tc = new TestCons( pipeline.getQueue() );
            //            pipeline.addConsumer( tc );
            new TestCons( pipeline, counter );
        }

        for ( int run = 0; run < 10; run++ )
        {
            pipeline.run();

            Thread.sleep( 10 );

            prod.restart();
        }

        pipeline.run();
        Pipeline.shutdownExecutorService();

        if ( counter.get() != 0 )
            throw new Exception();

        System.out.println( "OK" );
    }

    private static void run3NoExecutor( int consumerCount, int nRuns ) throws Throwable
    {
        Pipeline<String> pipeline = new Pipeline<>( 10 );
        Pipeline.useExecutorService = false;

        AtomicInteger counter = new AtomicInteger( 0 );
        new TestProd( pipeline, counter );

        for ( int i = 0; i < consumerCount; i++ )
            new TestCons( pipeline, counter );

        for ( int run = 0; run < nRuns; run++ )
            pipeline.run();

        Pipeline.shutdownExecutorService();
    }

    private static void run3Executor( int consumerCount, int nRuns ) throws Throwable
    {
        Pipeline<String> pipeline = new Pipeline<>( 10 );
        Pipeline.useExecutorService = true;

        AtomicInteger counter = new AtomicInteger( 0 );
        new TestProd( pipeline, counter );

        for ( int i = 0; i < consumerCount; i++ )
            new TestCons( pipeline, counter );

        for ( int run = 0; run < nRuns; run++ )
            pipeline.run();

        Pipeline.shutdownExecutorService();
    }

    // timing measurements with/without executor service
    private static void run3() throws Throwable
    {
        int nCons = 500; // consumers
        int nRuns = 100; // loops

        long start = System.currentTimeMillis();
        run3NoExecutor( nCons, nRuns );
        long dur = System.currentTimeMillis() - start;
        System.out.println( "dur no exec " + dur );

        start = System.currentTimeMillis();
        run3Executor( nCons, nRuns );
        dur = System.currentTimeMillis() - start;
        System.out.println( "dur exec " + dur );
    }

    // multiple producers
    private static void run4() throws Throwable
    {
        Pipeline<String> pipeline = new Pipeline<>( 10 );
        AtomicInteger counter = new AtomicInteger( 0 );

        int n = 10;
        for ( int i = 0; i < n; i++ )
        {
            new TestProd( pipeline, counter );
            new TestCons( pipeline, counter );
        }

        pipeline.run();
        Pipeline.shutdownExecutorService();

        if ( counter.get() != 0 )
            throw new Exception();

        System.out.println( "OK" );
    }
}
