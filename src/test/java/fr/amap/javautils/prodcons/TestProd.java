package fr.amap.javautils.prodcons;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import fr.amap.javautils.producerconsumer.Pipeline;
import fr.amap.javautils.producerconsumer.Producer;

public class TestProd extends Producer<String>
{
    private AtomicInteger counter;

    private static int idGen = 0;

    private int id;

    private Random rnd;

    public TestProd( Pipeline<String> p, AtomicInteger counter )
    {
        super( p );
        this.counter = counter;
        id = idGen++;
        rnd = new Random( System.currentTimeMillis() );
    }

    private int n;

    @Override
    protected String produce()
    {
        if ( n < 10 )
        {
            counter.incrementAndGet();
            n++;

            try
            {
                Thread.sleep( rnd.nextInt( 100 ) + 10 );
            }
            catch( InterruptedException e )
            {
            }

            return "int" + n;
        }
        return null;
    }

    public void restart()
    {
        n = 0;
        //System.out.println( "reset prod " + id );
    }
}
