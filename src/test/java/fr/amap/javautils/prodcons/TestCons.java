package fr.amap.javautils.prodcons;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import fr.amap.javautils.producerconsumer.Consumer;
import fr.amap.javautils.producerconsumer.Pipeline;

public class TestCons extends Consumer<String>
{
    private AtomicInteger counter;

    private static int idGen = 0;

    private int id;

    private Random rnd;

    public TestCons( Pipeline<String> p, AtomicInteger counter )
    {
        super( p );
        this.counter = counter;
        id = idGen++;
        rnd = new Random( System.currentTimeMillis() );
    }

    @Override
    protected void consume( String x )
    {
        counter.decrementAndGet();
        try
        {
            Thread.sleep( rnd.nextInt( 100 ) + 10 );
        }
        catch( InterruptedException e )
        {
        }
    }
}
