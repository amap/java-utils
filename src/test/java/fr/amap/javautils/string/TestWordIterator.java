package fr.amap.javautils.string;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class TestWordIterator
{
    public static void test( String in, String[] out )
    {
        WordIterator wi = new WordIterator( ' ' );
        wi.reset( in );
        int i = 0;
        while ( wi.hasNext() )
        {
            String w = wi.next();
            //            if ( i != wi.index() )
            //                throw new Exception( "input='" + in + "' exp=" + Arrays.toString( out ) );
            assertEquals( i, wi.index() );

            //            if ( !w.equals( out[ i ] ) )
            //                throw new Exception( "input='" + in + "' exp=" + Arrays.toString( out ) );
            assertTrue( w.equals( out[ i ] ) );

            i++;
        }

        //        if ( wi.hasNext() || wi.index() != out.length - 1 )
        //            throw new Exception( "input='" + in + "' exp=" + Arrays.toString( out ) );
        assertFalse( wi.hasNext() );
        assertEquals( wi.index(), out.length - 1 );
    }

    @Test
    public void test()
    {
        test( "", new String[] {} );
        test( " ", new String[] {} );
        test( "  ", new String[] {} );

        test( "a", new String[] { "a" } );
        test( " a", new String[] { "a" } );
        test( "  a", new String[] { "a" } );
        test( "a ", new String[] { "a" } );
        test( "a  ", new String[] { "a" } );
        test( " a ", new String[] { "a" } );
        test( "  a ", new String[] { "a" } );
        test( " a  ", new String[] { "a" } );
        test( "  a  ", new String[] { "a" } );

        test( "a b", new String[] { "a", "b" } );
        test( " a b", new String[] { "a", "b" } );
        test( "  a b", new String[] { "a", "b" } );
        test( "a b ", new String[] { "a", "b" } );
        test( "a b  ", new String[] { "a", "b" } );
        test( " a b  ", new String[] { "a", "b" } );
        test( "  a b  ", new String[] { "a", "b" } );

        test( "a  b", new String[] { "a", "b" } );
        test( " a  b", new String[] { "a", "b" } );
        test( "  a  b", new String[] { "a", "b" } );
        test( "a  b ", new String[] { "a", "b" } );
        test( "a  b  ", new String[] { "a", "b" } );
        test( " a  b  ", new String[] { "a", "b" } );
        test( "  a  b  ", new String[] { "a", "b" } );

        test( "aa", new String[] { "aa" } );
        test( " aa", new String[] { "aa" } );
        test( "  aa", new String[] { "aa" } );
        test( "aa ", new String[] { "aa" } );
        test( "aa  ", new String[] { "aa" } );
        test( " aa ", new String[] { "aa" } );
        test( "  aa ", new String[] { "aa" } );
        test( " aa  ", new String[] { "aa" } );
        test( "  aa  ", new String[] { "aa" } );

        test( "aa bb", new String[] { "aa", "bb" } );
        test( " aa bb", new String[] { "aa", "bb" } );
        test( "  aa bb", new String[] { "aa", "bb" } );
        test( "aa bb ", new String[] { "aa", "bb" } );
        test( "aa bb  ", new String[] { "aa", "bb" } );
        test( " aa bb  ", new String[] { "aa", "bb" } );
        test( "  aa bb  ", new String[] { "aa", "bb" } );

        test( "aa  bb", new String[] { "aa", "bb" } );
        test( " aa  bb", new String[] { "aa", "bb" } );
        test( "  aa  bb", new String[] { "aa", "bb" } );
        test( "aa  bb ", new String[] { "aa", "bb" } );
        test( "aa  bb  ", new String[] { "aa", "bb" } );
        test( " aa  bb  ", new String[] { "aa", "bb" } );
        test( "  aa  bb  ", new String[] { "aa", "bb" } );
    }

}
